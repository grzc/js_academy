let startButton = document.getElementById('startButton');
let stopButton = document.getElementById('stopButton');
let pauseButton = document.getElementById('pauseButton');
let label = document.getElementById('label');
let counter = 0;
let timer;

startButton.addEventListener('click', () => {
    timer = setInterval(() => {
        counter = Math.trunc(performance.now() / 1000);
        label.innerText = counter;
    }, 1000);
})

stopButton.addEventListener('click', () => {
    clearInterval(timer);
    counter = 0;
    label.innerText = "Licznik wyzerowany: " + counter;
})

pauseButton.addEventListener('click', () => {
    clearInterval(timer);
})

setTimeout(() => {
    alert("Czas na obiad");
}, 15000);